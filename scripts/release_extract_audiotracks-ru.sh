#!/bin/sh

PREFIX=`dirname $0`
BLENDFILE="${PREFIX}/../project-v9.blend"

cd $HOME/

# Voice
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 11
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 12
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 13
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 14
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 15

# Music
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 17
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 18

# FX
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 1
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 2
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 3
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 4
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 5
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 6
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 7
