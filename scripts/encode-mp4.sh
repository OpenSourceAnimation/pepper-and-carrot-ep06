PREFIX=`dirname $0`

#INPUT=$1
#OUTPUT=$2
#INPUT=/home/konstantin/temp/pepper-voronezh/00-lossless.avi
INPUT=$PREFIX/../render/project-v10-en.blend.avi

#OUTPUT=/home/konstantin/temp/pepper-voronezh/pepper-voronezh.mp4
OUTPUT=$PREFIX/../render/pepper.mp4

OPTIONS="-c:v libx264 -preset slow -b:v 4000k -x264-params keyint=5 -c:a aac -b:a 224k $3"

ffmpeg -y -i "${INPUT}" ${OPTIONS} -pass 1 -f mp4 /dev/null
ffmpeg -y -i "${INPUT}" ${OPTIONS} -pass 2 "${OUTPUT}" 
