Source files for **Pepper and Carrot Motion Comic: Episode 6**

You can fetch contents of this directory using RSync:

```
rsync -avz --exclude render rsync://archive.morevnaproject.org/sources/pepper-and-carrot-ep6/ ~/pepper-and-carrot-ep6/
```